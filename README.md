# **Number Plate Detection**
## Project Description
- **Domain** - Security and Surveillance
- **Objective** - To prevent unknown and potentially suspicious vehicles from trespassing into certain restricted premises.
- **Overview** -
A camera will capture the picture of the number plate of the vehicle. If the license plate number is present in the database then the access will be granted. If not, the app will ask the user either to grant one time access or add the number into database and give a permanent access. If no access is to be granted, the vehicle will be detected as a trespasser and will be forbidden from entering.
- **Application areas** - Housing societies,corporate offices and IT parks etc.
## Contributing in the Project
1. Select an issue in the area of your interest/expertise and try to complete it.
2. Solve the problems/errors reported by other contributors.
3. Any suggestions for adding features, modifications in the current model etc are welcome!
4. Help in creating a professional documentation for the entire project.
## How to complete the issues?
#### Selecting an issue
1. Read the objectives,goals milestones and other details before selecting the issue.
2. Post a comment in the issue you want to work on.
#### Completing the assigned task.
1. Clone the repository.
2. Make a branch and give branch name same as the issue you are working on.
3. Complete the tasks.
4. In case of codes, test it [here](https://hub.docker.com/r/shunyaos/shunya-armv7).
#### Objectives and goals
1. After the tasks are complete, verify if they meet the requirements and fulfill the goals.
2. Modify the work if necessary.
3. Make a merge request.
#### Issue completion
1. After the work is reviewed and accepted, it is merged by the maintainer.
2. You can now take the next issue and work on it!
## Deliverables
- Android Application
## What will we be doing??
1. **Image Preprocessing** - Segmentation, Character Recognition
2. **Database Creation** - A customised dataset for every user with features to add,delete and update the entries.
3. **Training and Testing** - Build an entire robust ML model to take image input and display its access specifier. Analyse using confusion matrix.
4. **Android App** - Build an android app with proper features and user friendly GUI for the end user. 
## Maintainer and Contributors
1. Maintainer - Mrunal Tambe @Mrunal-Tambe
2. Gaurav Bansal @Gaurav561 
3. Shreyas Chatterjee @Shreyas200188
4. Aman Rangapur @aman.rangapur987

## Perks
1. You get to work in your areas of interest within your project.
2. Your ideas and suggestions will be valued.
3. Top contributors can get the opportunity of being a domain lead.
4. Get an experience of real time industrial project.
Apart from this the project will surely add value to your resume!!!

**Looking forward to large participation and having you all onboard in this dynamic environment!!!**

**Let's all work together to accomplish this project!**





